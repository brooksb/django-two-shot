from accounts.views import login_view, user_logout, user_signup
from django.urls import path

urlpatterns = [
    path("login/", login_view, name="login"),
    path("logout/", user_logout, name="logout"),
    path("signup/", user_signup, name="signup"),
]
